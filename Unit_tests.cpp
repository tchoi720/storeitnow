//
//  Unit_tests.cpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#include <stdio.h>
#include "unit_test_framework.h"
#include "Unit.hpp"

TEST(test_title) {
    Unit sample("Here we go", 1);
    char ti[100] = sample.getTitle();
    char correct[100] = "Here we go";
    ASSERT_TRUE(ti == correct);
}







