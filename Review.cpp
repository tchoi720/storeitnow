//
//  Review.cpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#include "Review.hpp"

Review::Review(std::string& description, Unit* obj) {
    message = description;
    object = obj;
    rating = 0;
    reviewer = obj->getOwner();
}

void Review::setRating(int r) {
    rating = r;
}

int Review::getRating() {
    return rating;
}

Account* Review::getReviewer() {
    return this->reviewer;
}

Unit* Review::getUnit() {
    return object;
}

void Review::deletePost() {
    delete this;
}


ReviewList::ReviewList() {
    first = nullptr;
    reviews = nullptr;
    size = 0;
    one = false;
}

void ReviewList::addReview(Node* r) {
    if (size == 0) {
        first = r;
        reviews = first;
        reviews->unit_case = r->unit_case;
        reviews->next = nullptr;
    }
    reviews->unit_case = r->unit_case;
    r->next = nullptr;
}

Node* ReviewList::getReviews(int pos) {
    Node* ptr = first;
    int ct = 0;
    while (ct != pos) {
        ptr = ptr->next;
        ct++;
    }
    return ptr;
}

void ReviewList::removeReview(Node* r) {
    Node* ptr = first;
    Node* hold = first;
    while (ptr->unit_case->getUnit()->getTitle() !=
           r->unit_case->getUnit()->getTitle()) {
        hold = ptr;
        ptr = ptr->next;
    }
    hold = ptr->next;
    ptr = nullptr;
}

std::vector<Review*> ReviewList::findRatings(int rating) {
    std::vector<Review*> rated;
    Node* ptr = first;
    while (ptr != nullptr) {
        if (ptr->unit_case->getRating() == rating) {
            rated.push_back(ptr->unit_case);
        }
        ptr = ptr->next;
    }
    return rated;
}


bool ReviewList::onlyOne() {
    if (size == 1) {
        return true;
    }
    return false;
}

std::string ReviewList::getTitle() {
    return title;
}


bool ReviewList::none() {
    if (size == 0 || reviews == nullptr) {
        return true;
    }
    return false;
}

bool ReviewList::removeReviews() {
    reviews = nullptr;
    first = nullptr;
    if (reviews == nullptr) {
        return true;
    }
    return false;
}

double ReviewList::calculateRating() {
    int r = 0;
    Node* ptr = reviews;
    while (ptr != nullptr) {
        r += ptr->unit_case->getRating();
        ptr = ptr->next;
    }
    return r / this->size;
}

Node* ReviewList::getFirst() {
    return first;
}

int ReviewList::getSize() {
    return size;
}


int ReviewList::getRank() {
    return rank;
}

void ReviewList::setRank(int newRank) {
    rank = newRank;
}


ReviewDirectory::ReviewDirectory() {
    size = 0;
}

void ReviewDirectory::addUnit(ReviewList* rev) {
    allReviews.push_back(rev);
}

void ReviewDirectory::removeUnit(std::string name) {
    for (int i = 0; i < (int) allReviews.size(); ++i) {
        if (name == allReviews.at(i)->getTitle()) {
            delete allReviews.at(i);
            break;
        }
    }
};

void ReviewDirectory::sortUnitReviews() {
    ReviewList* best = allReviews.at(0);
    std::vector<ReviewList*> sorted;
    for (int i = 0; i < (int) allReviews.size(); ++i) {
        if (best->getRank() < allReviews.at(i)->getRank()) {
            best = allReviews.at(i);
            sorted.push_back(best);
        }
    }
    allReviews = sorted;
}

void ReviewDirectory::getRanks() {
    std::vector<ReviewList*> hold;
    hold.push_back(allReviews.at(0));
    for (int x = 0; x < (int) allReviews.size(); x++) {
        for (int y = 0; y < (int) hold.size(); ++y) {
            if (hold.at(y)->calculateRating() > allReviews.at(x)->calculateRating()) {
                hold.push_back(allReviews.at(x));
                std::swap(hold.at(x), hold.at(y));
    }
            else if (hold.at(y)->calculateRating() == allReviews.at(x)->calculateRating()) {
                if (hold.at(y)->getSize() > allReviews.at(x)->getSize()) {
                    hold.push_back(allReviews.at(x));
                    std::swap(hold.at(x), hold.at(y));
                }
            }
}
        hold.push_back(allReviews.at(x));
}
    for (int i = 0; i < allReviews.size(); ++i) {
        for (int j = 0; j < (int) hold.size(); ++i) {
            if (hold.at(j)->getTitle() == allReviews.at(i)->getTitle()) {
                allReviews.at(i)->setRank(j+1);
                }
    }
    }
}

bool ReviewDirectory::empty() {
    if (size == 0) {
        return true;
    }
    return false;
}

int ReviewDirectory::getSize() {
    return size;
}

double ReviewDirectory::FindRating(Review* find) {
    Node* ptr = allReviews.at(0)->getFirst();
    for (int i = 0; i < (int) allReviews.size(); i++) {
        while (ptr != nullptr) {
            ptr = allReviews.at(i)->getFirst();
            if (ptr->unit_case->getUnit()->getTitle() ==
                find->getUnit()->getTitle()) {
                return ptr->unit_case->getRating();
            }
            ptr = ptr->next;
        }
    }
}
