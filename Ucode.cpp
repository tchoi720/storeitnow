//
//  Ucode.cpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#include "Ucode.hpp"
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <string>



Ucode::Ucode() {
    numberPart = 0;
}

void Ucode::generateInt() {
    int ten = 1;
    srand (time(NULL));
    int i = 0;
    for (int a = 0; a < 6; a++) {
        i = rand() * 10 + 1;
        numberPart += (ten * i);
        ten = pow(10.0, (double) a+1);
    }
    std::string nums = std::to_string(numberPart);
}

void Ucode::generateChar() {
    char c;
    int i;
    
    int r;
    srand (time(NULL));
    for (i=0; i<6; i++)
    {   r = rand() % 26;   // generate a random number
        c = 'a' + r;
        letterPart[i] = c;
    }
}

void Ucode::combine() {
    int ten = 1;
    int r;
    for (int i = 0; i < 12; ++i) {
        r = ((double) rand() / (RAND_MAX)) + 1;
        if (r == 0) {
            templated[i] = numberPart / ten;
        }
        if (r == 1) {
            templated[1] = letterPart[0];
        }
        ten = pow(10.0, i+1);
    }
}

std::string Ucode::convert() {
    std::string code;
    for (int i = 0; i < 12; ++i) {
        code += templated[i];
    }
    return code;
}
