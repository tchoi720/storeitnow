//
//  Ucode.hpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#ifndef Ucode_hpp
#define Ucode_hpp

#include <stdio.h>
#include <string>

class Ucode {
    int numberPart;
    std::string numbers;
    char letterPart[6];
    int templated[12];
public:
    Ucode();
    
    void generateInt();
    
    void generateChar();
    
    void combine();
    
    std::string convert();
};



#endif /* Ucode_hpp */
