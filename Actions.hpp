//
//  Actions.hpp
//  StoreIt
//
//  Created by Tim Choi on 2/6/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#ifndef Actions_hpp
#define Actions_hpp
#include "database.hpp"
#include "Unit.hpp"
#include "Account.hpp"

#include <stdio.h>



class UserInterface {
    Account* user;
public:
    UserInterface();   //Same as "opening" the app
    
    void CreateAccount();
    
    void LogIn();
    
    void SelectOption();
    
    void postFile();
    
    void accessFile();
    
    void getPermission();
    
    void followUser();
    
    void unfollow();
    
    void postReview();
    
    void removeReview();
    
    void signOut();
    
    void viewAccount();
    
    void viewPostings();
    
    void changeAccount();
    
    void limitViews();
    
    void regulateFollowers();
    
    void logOut();
    
    UArray* findSection();
    
    Unit* findUnit();
};



#endif /* Actions_hpp */
