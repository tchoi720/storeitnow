//
//  Password.hpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#ifndef Password_hpp
#define Password_hpp
#include <string>

#include <stdio.h>


class password {
    int length;
    std::string code;
    int numSide;
    char LetterSide[100];
    bool valid;
    char templated[100];
public:
    password();
    
    password(std::string users);
    
    int getLength();
    
    bool isValid(std::string users);
    
    void replacePass(std::string users2);
    
    void generateNumbers();
    
    void generateLetters();
    
    void combine();
    
    std::string generatePass();
};

#endif /* Password_hpp */
