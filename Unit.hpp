//
//  Unit.hpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#ifndef Unit_hpp
#define Unit_hpp
#include <fstream>
#include <string>
#include "Account.hpp"
#include <vector>
#include "Review.hpp"

#include <stdio.h>



class Unit {
    char title;
    
    std::fstream& file;
    
    char timeMark;
    
    int times_accessed;
    
    bool open;
    
    bool restricted;
    
    char link[1000];
    
    int waiting;
    
    bool close;
    
    Account* owner;
    
    std::vector<Account*> allowed_users;
    
    int reviews[5];
public:
    Unit(char* name, int mode); //Uploads an existing file
    
    char getTitle(); //gets title
    
    Account* getOwner(); //gets owner info
    
    int getNumWaiting(); //gets number of people waiting
    
    char* getLink(); //gets link
    
    bool isPrivate(); //is open to public
    
    bool isOpen(); //is open for use
    
    void openfile(std::ofstream& out, char* name); //opens file
    
    void closeFile(std::ofstream& out); //enables user to close file
    
    int getTimesUsed(); //gets number of times accessed
        
    void remove(); //deleted
    
    void openLink(); //Opens URL

    void openUnit(Account* user);  //Makes file public
    
    void closeUnit();  //Makes file private
    
    void addUser(Account* user); //Adds user to list
    
    void removeUser(Account* user); //Removes user to list
        
    void getReviewDist(ReviewList* list);
    
    bool changeClosed();
    
    void uploadToDrive();
    
    void closeAccess();
    
    void openAccess();

};

std::ostream & operator<<(std::ostream& out, Unit& u);

std::ostream & operator||(std::ostream& out2, char& mark);


#endif /* Unit_hpp */
