//
//  Unit.cpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//
#include <cstdlib>
#include "Unit.hpp"
#include <stdio.h>
#include <curl/curl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>


#include <fcntl.h>
#include <errno.h>
#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif


#define LOCAL_FILE      "/tmp/uploadthis.txt"
#define UPLOAD_FILE_AS  "while-uploading.txt"
#define REMOTE_URL      "ftp://example.com/"  UPLOAD_FILE_AS
#define RENAME_FILE_TO

#include <fstream>
#include <string>
#include "Account.hpp"
#include <vector>
#include "Review.hpp"
#include <iostream>





Unit::Unit(char* name, int mode);
//Uploads an existing file

char Unit::getTitle() {
    return title;
} //gets title

Account* Unit::getOwner() {
    return owner;
}

int Unit::getNumWaiting() {
    return waiting;
} //gets number of people waiting

char* Unit::getLink() {
    return link;
} //gets link

bool Unit::isPrivate() {
    if (restricted) {
        return true;
    }
    return false;
} //is open to public

bool Unit::isOpen() {
    if (open) {
        return true;
    }
    return false;
} //is open for use

void Unit::openfile(std::ofstream& out, char* name) {
    out.open(name);
    if (close) {
        closeFile(out);
    }
}

void Unit::openAccess() {
    close = false;
}


void Unit::closeAccess() {
    close = true;
}

void Unit::closeFile(std::ofstream& out) {
    out.close();
}

int Unit::getTimesUsed() {
    return times_accessed;
}

void Unit::remove() {
    delete this;
} //deleted

void Unit::openLink() {
    std::string l;
    std::string cmd = "Open " +
    while (*link != "") {
        l += *link;
        link++;
    }
    cmd += l;
    char lk[cmd.length() + 1];
    strcpy(lk, cmd.c_str());
    int i = system(lk);
    printf ("The value returned was: %d.\n",i);
    getchar();
} //Opens URL

void Unit::openUnit(Account* user) {
    allowed_users = user->followers;
}

void Unit::closeUnit() {
    std::vector<Account*> sub;
    sub = allowed_users;
    allowed_users = {};
}

void Unit::addUser(Account* user) {
    allowed_users.push_back(user);
} 

void Unit::removeUser(Account* user) {
    for (int i = 0; i < (int) allowed_users.size(); ++i) {
        if (user->getName() == allowed_users.at(i)->getName()) {
            allowed_users.erase(allowed_users.begin() + i);
            break;
        }
}
}
//Removes user to list

void Unit::getReviewDist(ReviewList* list) {
    Node* i = list->getReview();
    while (i != nullptr) {
        switch (i->getRating()) {
            case 1:
                reviews[0]++;
            case 2:
                reviews[1]++;
            case 3:
                reviews[2]++;
            case 4:
                reviews[3]++;
            case 5:
                reviews[4]++;
            default:
                break;
        }
    }
    std::cout << "1 star: " << reviews[0] << std::endl;
    std::cout << "2 star: " << reviews[1] << std::endl;
    std::cout << "3 star: " << reviews[2] << std::endl;
    std::cout << "4 star: " << reviews[3] << std::endl;
    std::cout << "5 star: " << reviews[4] << std::endl;
}


std::ostream & operator<<(std::ostream& out, Unit& u) {
    out << u.getTitle() << std::endl;
    out << u.getLink() << std::endl;
    out << u.getOwner() << std::endl;
}

std::ostream & operator||(std::ostream& out2, char& mark) {
    out2 << mark << std::endl;
    out2 << "Time:" << &time << std::endl;
}

