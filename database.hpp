//
//  database.hpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#ifndef database_hpp
#define database_hpp

#include <stdio.h>
#include <vector>
#include "Unit.hpp"
#include <string>
#include "Review.hpp"
#include "Account.hpp"


struct node {
    Account* user;
    node* next;
};

class waitlist {
    node* wait;
    int numWaiting;
    bool empty;
    bool one;
    node* first;
public:
    waitlist();
    
    void addUser(node* u);
    
    void removeUser(node* u);
    
    void sortByTime();
    
    void swap(node* pos1, node* pos2);
    
    bool isEmpty();
    
    bool onlyOne();
};

class UArray {
    std::vector<Unit*> file_group;
    int number_of_units;
    Unit* first;
    bool empty;
    std::string title;
    char* mark;
    bool changed;
    
public:
    UArray(Unit* one, std::string name);      //Creates a new Section
    
    void addUnit(Unit* add);
    
    void removeUnit(Unit* re);
        
    void lock(Unit* u);
    
    void lockAll();
    
    void unlock(Unit* u);
    
    void unlockAll();

    Unit* find(char& name);
    
    int getNumberOfUnits();
    
    bool isEmpty();
    
    std::string getTitle();
    
    void printTimeStamp();
    
    void printFiles();
    
    bool isUpdated();
    
    void sortByAlph();
    
    void sortByRank();
};


class database {
    std::vector<UArray*> sections;
    int numOfSecs;
    bool empty;
    bool updated;
    
public:
    database();
        
    void addSection(UArray* title);
    
    void deleteSection(UArray* title);
    
    bool isEmpty();
    
    bool isUpdated();
    
    int getNumOfSecs();
    
    void sortByAlph();
    
    void sortByRank();
    
    UArray* find(UArray* title);
    
    void viewSections();
};

#endif /* database_hpp */
