//
//  Password.cpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#include "Password.hpp"
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <string>





password::password() {
    generatePass();
}

password::password(std::string users) {
    if (this->isValid(users)) {
        code = users;
    }
    throw 1;
}

int password::getLength() {
    return length;
}

bool password::isValid(std::string users) {
    for (int i = 0; i < (int) users.length(); ++i) {
        if (users[i] >= 62) {
            return false;
        }
    }
    return true;
}

void password::replacePass(std::string users2) {
    if (this->isValid(users2)) {
        code = users2;
    }
    throw 1;
}

void password::generateNumbers() {
    int ten = 1;
    srand (time(NULL));
    int i = 0;
    for (int a = 0; a < 6; a++) {
        i = rand() * 10 + 1;
        numSide += (ten * i);
        ten = pow(10.0, (double) a+1);
    }
    std::string nums = std::to_string(numSide);
}

void password::generateLetters() {
    char c;
    int i;
    
    int r;
    srand (time(NULL));
    for (i=0; i<6; i++)
    {   r = rand() % 26;   // generate a random number
        c = 'a' + r;
        LetterSide[i] = c;
    }
}

void password::combine() {
    int ten = 1;
    int r;
    for (int i = 0; i < 12; ++i) {
        r = ((double) rand() / (RAND_MAX)) + 1;
        if (r == 0) {
            templated[i] = numSide / ten;
        }
        if (r == 1) {
            templated[1] = LetterSide[0];
        }
        ten = pow(10.0, i+1);
    }
}

std::string password::generatePass() {
    generateNumbers();
    generateLetters();
    combine();
    std::string code;
    for (int i = 0; i < 12; ++i) {
        code += templated[i];
    }
    return code;
}
