//
//  Review.hpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#ifndef Review_hpp
#define Review_hpp
#include "Unit.hpp"
#include <string>
#include "Account.hpp"
#include <vector>

#include <stdio.h>


class Review {
    Unit* object;
    std::string message;
    int rating;
    Account* reviewer;
public:
    Review(std::string& description, Unit* obj);
    
    void setRating(int r);
    
    int getRating();
    
    Account* getReviewer();
    
    Unit* getUnit();
    
    void deletePost();
};

struct Node {
    Review* unit_case;
    Node* next;
};

class ReviewList {
    std::string title;
    Node* reviews;
    bool one;
    int size;
    int rank;
    Node* first;
public:
    ReviewList();
        
    std::string getTitle();
    
    Node* getReview() {
        return reviews;
    }
    
    void addReview(Node* r);
    
    Node* getReviews(int pos);
    
    void removeReview(Node* r);
    
    bool onlyOne();
    
    bool none();
    
    bool removeReviews();
    
    double calculateRating();
    
    int getRank();
    
    void setRank(int newRank);
    
    Node* getFirst();
    
    int getSize();
    
    std::vector<Review* > findRatings(int rating);
    
};

class ReviewDirectory {
    std::vector<ReviewList*> allReviews;
    
    int size;
    
public:
    ReviewDirectory();
    
    void addUnit(ReviewList* rev);
    
    void removeUnit(std::string name);
    
    void sortUnitReviews();
    
    bool empty();
    
    int getSize();
 
    void getRanks();
    
    double FindRating(Review* find);
};

std::ostream & operator<<(std::ostream& out, Review* rev);

#endif /* Review_hpp */
*