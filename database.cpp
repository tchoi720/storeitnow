//
//  database.cpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#include "database.hpp"
#include <ctime>
#include <iostream>


waitlist::waitlist() {
    numWaiting = 0;
    empty = true;
    wait = nullptr;
    one = false;
    first = nullptr;
}

void waitlist::addUser(node* u) {
    if (one) {
        wait = first = u;
        wait->user = u->user;
        wait->next = nullptr;
    }
    else {
        wait->user = u->user;
        wait->next = nullptr;
    }
}

void waitlist::removeUser(node* u) {
    if (one) {
        first = nullptr;
    }
    else if (!one && !empty) {
        node* ptr = first;
        node* hold = u;
        while (ptr != nullptr) {
            if (ptr->next == u) {
                ptr = hold;
                ptr->next = nullptr;
                ptr = ptr->next->next;
            }
        }
    }
}

void waitlist::sortByTime() {
    node* sorted;
    node* ptr;
    node* ptr1;
    int year1 = first->user->getStamp()->year;
    node* n;
    while (ptr != nullptr) {
    while (ptr1 != nullptr) {
        if (year1 < ptr1->user->getStamp()->year) {
            year1 = ptr1->user->getStamp()->year;
            sorted->user = ptr1->user;
            sorted->next = nullptr;
        }
        ptr1 = ptr1->next;
    }
        ptr = ptr->next;
        n = sorted;
}
}

void waitlist::swap(node* pos1, node* pos2) {
    node* ptr = first;
    node* ptr2;
    int p1;
    int p2;
    while (ptr != pos1) {
        p1++;
        ptr = ptr->next;
    }
    ptr2 = first;
    while (ptr2 != pos2) {
        p2++;
        ptr2 = ptr2->next;
    }
    if (p2 == p1 || p2 < p1) {
        return;
    }
    else {
        Account* hold;
        hold = ptr->user;
        ptr->user = ptr2->user;
        ptr2->user = hold;
    }
}

bool waitlist::isEmpty() {
    if (wait == nullptr) {
        return true;
    }
    return false;
}

bool waitlist::onlyOne() {
    if (first && !first->next) {
        return true;
    }
    return false;
}


UArray::UArray(Unit* one, std::string name) {
    time_t now = time(0);
    char* dt = ctime(&now);
    first = one;
    number_of_units = 0;
    empty = true;
    mark = dt;
    changed = false;
    title = name;
}     //Creates a new Section

void UArray::addUnit(Unit* add) {
    file_group.push_back(add);
}

void UArray::removeUnit(Unit* re) {
    if (empty) {
        return;
    }
    for (int i = 0; i < (int) file_group.size(); ++i) {
        if (file_group.at(i)->getTitle() == re->getTitle()) {
            file_group.erase(file_group.begin() + i);
        }
    }
    if (number_of_units == 0) {
        empty = true;
    }
}

void UArray::lock(Unit* u) {
    for (int i = 0; i < (int) file_group.size(); ++i) {
        if (file_group.at(i) == u) {
            file_group.at(i)->closeAccess();
        }
    }
}
void UArray::lockAll() {
    for (int i = 0; i < (int) file_group.size(); ++i) {
        file_group.at(i)->closeAccess();
    }
}

void UArray::unlock(Unit* u) {
    for (int i = 0; i < (int) file_group.size(); ++i) {
        if (file_group.at(i) == u) {
            file_group.at(i)->openAccess();
        }
    }
}

void UArray::unlockAll() {
    for (int i = 0; i < (int) file_group.size(); ++i) {
        file_group.at(i)->openAccess();
    }
}

Unit* UArray::find(char& name) {
    for (int i = 0; i < (int) file_group.size(); ++i) {
        if (file_group.at(i)->getTitle() == name) {
            return file_group.at(i);
        }
    }
}

int UArray::getNumberOfUnits() {
    return number_of_units;
}

bool UArray::isEmpty() {
    if (empty) {
        return true;
    }
    return false;
}

std::string UArray::getTitle() {
    return title;
}

void UArray::printTimeStamp() {
    std::cout << *mark << std::endl;
}

void UArray::printFiles() {
    for (int i = 0; i < (int) file_group.size(); ++i) {
        std::cout << file_group.at(i) << std::endl;
    }
    std::cout << std::endl;
}

bool UArray::isUpdated() {
    if (changed) {
        return true;
    }
    return false;
}

void UArray::sortByAlph() {
    std::vector<Unit* > sortedArray;
    int first = file_group.at(0)->getTitle();
    for (int i = 0; i < (int) file_group.size(); ++i) {
        for (int j = 0; j < file_group.size(); ++j) {
            
        }
    }
}

void UArray::sortByRank();

database::database() {
    numOfSecs = 0;
    empty = true;
    updated = false;
}

void database::addSection(UArray* title);

void database::deleteSection(UArray* title);

bool database::isEmpty();

bool database::isUpdated();

int database::getNumOfSecs();

void database::sortByAlph();

void database::sortByRank();

UArray* database::find(UArray* title);

void database::viewSections();
