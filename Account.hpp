//
//  Account.hpp
//  StoreIt
//
//  Created by Tim Choi on 2/5/20.
//  Copyright © 2020 Tim Choi. All rights reserved.
//

#ifndef Account_hpp
#define Account_hpp

#include <stdio.h>
#include <string>
#include "Password.hpp"
#include <vector>
#include "Unit.hpp"



std::vector<std::string> all_usernames;

class Account {
    std::string name;
    std::string usermame;
    char* began;
    password* pass;
    int numPosts;
    bool on;
    bool accepted;
    int numOfFollowers;
    int numOfFollowing;
    std::vector<Account*> following;
public:
    std::vector<Unit*> posts;
    
    std::vector<Account*> followers;
    
    Account(std::string inputName);
    
    std::string getUser();
    
    timeStamp getStamp();
    
    std::string getName();
    
    bool isOn();
    
    void changePassword();
    
    void changeUsername();
    
    void addFriend();
    
    void removeFriend();
    
    void addFollowing();
    
    void removeFollowing();
    
    void acceptFriend();
    
    void addUnit();
    
    void removeUnit();
    
    Unit* findUnit(std::string title);
    
    void deactivate();
    
    void godelete();
    
    void clearPage();
    
    void blockFriend();
    
    void blockAll();
    
    void unblockFriend();
    
    void unblockAll();
    
    void requestFriend();
    
    void blockUnit();
    
    void unblockUnit();
    
    void addToArchive(std::string unit);
    
    void removeFromArchive(std::string unit);
};

#endif /* Account_hpp */
